# TroopMaster Pocket Certificate Printing

When a youth in the Boy Scouts of America earns certain awards (rank
advancements, merit badges, etc.), they receive a "pocket certificate"
showing that they earned the award.  BSA sells printable sheets of many of
these certificates.

Unfortunately, the [TroopMaster][] software--used by many troops to manage
their advancements--doesn't really have a good way to print to BSA's
printable sheets.  Its certificate printing needs blank cardstock and a
color printer because it prints the entire card, not just the fields
unique to each youth.

  [TroopMaster]: http://troopmaster.com/

This software exists to bridge that gap.  It can take a TroopMaster Court
of Honor CSV export and generate PDFs to be printed onto BSA's certificate
sheets.


## Prerequisites

You need the [dateutil][] Python module installed, as well as the
[pdftk][] software.

  [dateutil]: https://github.com/dateutil/dateutil/
  [pdftk]: http://www.pdftk.com


## Usage

In TroopMaster, go to the "Court of Honor w/4403" advancement report.  Set
the dates appropriately for your Court of Honor then click the "CSV
Export" button.  Save the generated CSV file.

Run the `make_templates` program, giving it your council, unit number, and
the CSV file you exported from TroopMaster.  e.g. for Example Area Council
Troop 123, you'd run:

    ./make_templates "Example Area Council" 123 COHExport.CSV

This will create a number of PDFs, one for each certificate sheet you
need.  Simply print those PDFs onto the sheets.


## Template Sources

The templates were downloaded from the following locations:

 * Scout (627522) - No source found yet.
 * Tenderfoot (33411) - https://mediafiles.scoutshop.org/m2pdf/33411_template.pdf
 * Second Class (33412) - https://mediafiles.scoutshop.org/m2pdf/33412_template.pdf
 * First Class (33413) - https://mediafiles.scoutshop.org/m2pdf/33413_template.pdf
 * Star (33426) - https://mediafiles.scoutshop.org/m2pdf/33426_template.pdf
 * Life (33425) - https://mediafiles.scoutshop.org/m2pdf/33425_template.pdf
 * Eagle - Not available in printable sheets.
 * Merit Badge (33414) - https://mediafiles.scoutshop.org/Media/FINAL_33414%282020%29%20MerBadgPktCert_Non-Printing.pdf
